// import * as Highcharts from 'highcharts';
// import 'highcharts/modules/sunburst';
import 'global-jsdom/register'

import * as Highcharts from 'highcharts';
import { Chart } from 'highcharts';
// Load module after Highcharts is loaded
import SunburstModule from 'highcharts/modules/sunburst';
import ExportingModule from 'highcharts/modules/exporting';
import fs from 'fs';

SunburstModule(Highcharts);
ExportingModule(Highcharts);

const container = document.createElement('div');
document.body.append(container);

const colors = Highcharts.getOptions().colors.slice();

const categories = [
    'Entreprise',
    'Immobilier',
    'Financier',
    'Autres'
];

const data = [
    {
        y: 50,
        color: colors[2],
        drilldown: {
            name: 'Entreprise',
            categories: [
                'Alain PP',
                'Veronique PP'
            ],
            data: [
                40,
                10
            ]
        }
    },
    {
        y: 50,
        color: colors[1],
        drilldown: {
            name: 'Immobilier',
            categories: [
                'Alain PP',
                'Veronique NP',
                'Alain US'
            ],
            data: [
                10,
                20,
                20
            ]
        }
    },
    {
        y: 50,
        color: colors[0],
        drilldown: {
            name: 'Financier',
            categories: [
                'Alain PP',
                'Veronique NP',
                'Alain US'
            ],
            data: [
                10,
                20,
                20
            ]
        }
    },
    {
        y: 50,
        color: colors[3],
        drilldown: {
            name: 'Autres',
            categories: [
                'Alain PP',
                'Veronique PP'
            ],
            data: [
                25,
                25
            ]
        }
    }
];
const firstSeriesData = [];
const secondSeriesData = [];
let i;
let j;
const dataLen = data.length;
let drillDataLen;
let brightness;


// Build the data arrays
for (i = 0; i < dataLen; i += 1) {

    // add browser data
    firstSeriesData.push({
        name: categories[i],
        y: data[i].y,
        color: data[i].color
    });

    // add version data
    drillDataLen = data[i].drilldown.data.length;
    for (j = 0; j < drillDataLen; j += 1) {
        brightness = 0.2 - (j / drillDataLen) / 5;
        secondSeriesData.push({
            name: data[i].drilldown.categories[j],
            y: data[i].drilldown.data[j],
            color: Highcharts.color(data[i].color).brighten(brightness).get()
        });
    }
}

// Create the chart
let initialised: boolean;

Highcharts.chart(container, {
    chart: {
        type: 'pie'
    },
    title: {
        text: 'Pie avec Highchart'
    },
    subtitle: {
        text: 'Source: <a href="http://statcounter.com" target="_blank">statcounter.com</a>'
    },
    plotOptions: {
        pie: {
            shadow: false,
            center: ['50%', '50%']
        }
    },
    tooltip: {
        valueSuffix: '%'
    },
    series: [{
        type: 'pie',
        name: 'Categories',
        data: firstSeriesData,
        size: '60%',
        dataLabels: {
            formatter() {
                return this.y > 5 ? this.point.name : null;
            },
            color: '#ffffff',
            distance: -30
        }
    }, {
        type: 'pie',
        name: 'Détails',
        data: secondSeriesData,
        size: '80%',
        innerSize: '60%',
        dataLabels: {
            formatter() {
                // display only if larger than 1
                return this.y > 1 ? '<b>' + this.point.name + ':</b> ' +
                    this.y : null;
            }
        },
        id: 'details'
    }],
    responsive: {
        rules: [{
            condition: {
                maxWidth: 400
            },
            chartOptions: {
                series: [{
                    type: 'pie'
                }, {
                    type: 'pie',
                    id: 'details',
                    dataLabels: {
                        enabled: false
                    }
                }]
            }
        }]
    }
}, (chart: Chart) => {
    console.log("Image chargée");
    if (!initialised) {
        initialised = true;
        fs.writeFile('sunburst.svg', chart.getSVG(), () => {
            console.log("Image exportée");
        });
    }
});

const data2 = [{
    id: '0.0',
    parent: '',
    name: ''
}, {
    id: '1.1',
    parent: '0.0',
    name: 'Entreprise',
    value: 50
}, {
    id: '1.2',
    parent: '0.0',
    name: 'Immobilier',
    value: 50
}, {
    id: '1.3',
    parent: '0.0',
    name: 'Financier',
    value: 50
}, {
    id: '1.4',
    parent: '0.0',
    name: 'Autres',
    value: 50
}, {
    id: '2.1',
    parent: '1.1',
    name: 'Alain PP',
    value: 40
}, {
    id: '2.2',
    parent: '1.1',
    name: 'Veronique PP',
    value: 10
}, {
    id: '2.3',
    parent: '1.2',
    name: 'Alain PP',
    value: 10
}, {
    id: '2.4',
    parent: '1.2',
    name: 'Veronique NP',
    value: 20
}, {
    id: '2.5',
    parent: '1.2',
    name: 'Alain US',
    value: 20
}, {
    id: '2.6',
    parent: '1.3',
    name: 'Alain PP',
    value: 10
}, {
    id: '2.7',
    parent: '1.3',
    name: 'Veronique NP',
    value: 20
}, {
    id: '2.8',
    parent: '1.3',
    name: 'Alain US',
    value: 20
}, {
    id: '2.9',
    parent: '1.4',
    name: 'Alain PP',
    value: 25
}, {
    id: '2.10',
    parent: '1.4',
    name: 'Veronique PP',
    value: 25
}];

// Splice in transparent for the center circle
const colors2 = Highcharts.getOptions().colors.slice();
colors2.splice(0, 0, 'white');

const container2 = document.createElement('div');
document.body.append(container2);

let initialised2: boolean;
Highcharts.chart(container2, {

    chart: {
        height: '100%'
    },

    title: {
        text: 'Sunburst avec HighChart'
    },
    colors: colors2,
    series: [{
        type: 'sunburst',
        data: data2,
        cursor: 'pointer',
        dataLabels: {
            format: '{point.name}',
            filter: {
                property: 'innerArcLength',
                operator: '>',
                value: 9
            },
            rotationMode: 'circular'
        },
        levels: [{
            level: 1,
            dataLabels: {
                filter: {
                    property: 'outerArcLength',
                    operator: '>',
                    value: 9
                }
            }
        }, {
            level: 2,
            // @ts-ignore
            colorByPoint: true,
        }, {
            level: 3,
            colorVariation: {
                key: 'brightness',
                to: -0.5
            }
        }]

    }],
    tooltip: {
        headerFormat: ''
    }
}, (chart: Chart) => {
    console.log("Image chargée");
    if (!initialised2) {
        initialised2 = true;
        fs.writeFile('sunburst2.svg', chart.getSVG(), () => {
            console.log("Image exportée");
        });
    }
});
